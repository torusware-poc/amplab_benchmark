# Copyright 2013 The Regents of The University California
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Prepare the big data benchmark on one or more EC2 or Redshift clusters.

   This script will copy the appropriately sized input data set from s3
   into the provided cluster or clusters.
"""

import subprocess
import sys
from sys import stderr
from optparse import OptionParser
import os
import time
from pg8000 import DBAPI
import pg8000.errors

### Runner ###
def parse_args():
  parser = OptionParser(usage="cluster_prepare_benchmark.py [options]")

  parser.add_option("-s", "--sparksql", action="store_true", default=False,
      help="Whether to include SparkSQL")
  parser.add_option("-m", "--impala", action="store_true", default=False,
      help="Whether to include Impala")
  parser.add_option("-r", "--redshift", action="store_true", default=False,
      help="Whether to include Redshift")
  parser.add_option("--hive", action="store_true", default=False,
      help="Whether to include Hive")
  parser.add_option("--hive-tez", action="store_true", default=False,
      help="Whether to include Hive")
  parser.add_option("--hive-cdh", action="store_true", default=False,
      help="Hive on CDH cluster")

  parser.add_option("-b", "--sparksql-host",
      help="Hostname of SparkSQL master node")
  parser.add_option("-u", "--sparksql-user",
      help="User to access SqprkSQL master node");
  parser.add_option("-y", "--sparksql-identity-file",
      help="SSH private key file to use for logging into SparkSQL node")
  parser.add_option("-i", "--sparksql-hadoop-home", 
      help="Hadoop path")
  parser.add_option("-t", "--sparksql-home",
      help="Spark home")
  parser.add_option("-e", "--sparksql-tmp", default="$HOME",
      help="Spark tmp dir shared between hosts")
  parser.add_option("-w", "--sparksql-master-url",
      help="spark://IP:PORT master url")

  parser.add_option("-o", "--benchmark-data", 
      help="Benchmark data path")

  parser.add_option("--skip-import", action="store_true", default=False,
      help="Assumes data is already loaded")

  (opts, args) = parser.parse_args()

  if not (opts.sparksql):
    parser.print_help()
    sys.exit(1)

  if opts.sparksql and (opts.sparksql_host is None or 
      opts.sparksql_identity_file is None or
      opts.sparksql_user is None or
      opts.sparksql_home is None or 
      opts.sparksql_hadoop_home is None or
      opts.benchmark_data is None or
      opts.sparksql_master_url is None):
    print >> stderr, \
        "SparkSQL requires user, hostname, identity file, spark home," \
        "spark master url, hadoop home and benchmark data path"
    sys.exit(1)

  return opts

# Run a command on a host through ssh, throwing an exception if ssh fails
def ssh(host, username, identity_file, command):
  subprocess.check_call(
      "ssh -t -o StrictHostKeyChecking=no -i %s %s@%s '%s'" %
      (identity_file, username, host, command), shell=True)

# Copy a file to a given host through scp, throwing an exception if scp fails
def scp_to(host, identity_file, username, local_file, remote_file):
  subprocess.check_call(
      "scp -q -o StrictHostKeyChecking=no -i %s '%s' '%s@%s:%s'" %
      (identity_file, local_file, username, host, remote_file), shell=True)

# Copy a file to a given host through scp, throwing an exception if scp fails
def scp_from(host, identity_file, username, remote_file, local_file):
  subprocess.check_call(
      "scp -q -o StrictHostKeyChecking=no -i %s '%s@%s:%s' '%s'" %
      (identity_file, username, host, remote_file, local_file), shell=True)

def prepare_sparksql_dataset(opts):
  def ssh_sparksql(command):
    command = "source $HOME/.bash_profile; %s" % command
    ssh(opts.sparksql_host, opts.sparksql_user, opts.sparksql_identity_file, command)

  if not opts.skip_import:
    print "=== IMPORTING BENCHMARK DATA FROM SOURCE ==="
    try:
      ssh_sparksql("%s/bin/hdfs dfs -mkdir /user/%s/benchmark" %
          (sparksql_user, opts.sparksql_hadoop_home))
    except Exception:
      pass # Folder may already exist

    ssh_sparksql(
      "%s/bin/hadoop distcp " \
      "%s/rankings/ " \
      "/user/%s/benchmark/rankings/" % (opts.sparksql_hadoop_home, opts.benchmark_data,
        opts.sparksql_user))

    ssh_sparksql(
      "%s/bin/hadoop distcp " \
      "%s/uservisits/ " \
      "/user/%s/benchmark/uservisits/" % (
        opts.sparksql_hadoop_home, opts.benchmark_data, opts.sparksql_user))

    ssh_sparksql(
      "%s/bin/hadoop distcp " \
      "%s/crawl/ " \
      "/user/%s/benchmark/crawl/" % (opts.sparksql_hadoop_home, opts.benchmark_data,
        opts.sparksql_user))

    # Scratch table used for JVM warmup
    ssh_sparksql(
      "%s/bin/hadoop distcp /user/%s/benchmark/rankings " \
      "/user/%s/benchmark/scratch" % (opts.sparksql_hadoop_home, opts.sparksql_user,
        opts.sparksql_user))

  scp_to(opts.sparksql_host, opts.sparksql_identity_file, opts.sparksql_user, "udf/url_count.py",
      "%s/url_count.py" % opts.sparksql_tmp)

  ssh_sparksql(
    "%s/bin/spark-sql --master %s " \
    "-e \"DROP TABLE IF EXISTS rankings; " \
    "CREATE EXTERNAL TABLE rankings (pageURL STRING, pageRank INT, " \
    "avgDuration INT) ROW FORMAT DELIMITED FIELDS TERMINATED BY \\\",\\\" " \
    "STORED AS TEXTFILE LOCATION \\\"/user/%s/benchmark/rankings\\\";\"" \
    % (opts.sparksql_home, opts.sparksql_master_url, opts.sparksql_user))

  ssh_sparksql(
    "%s/bin/spark-sql --master %s " \
    "-e \"DROP TABLE IF EXISTS scratch; " \
    "CREATE EXTERNAL TABLE scratch (pageURL STRING, pageRank INT, " \
    "avgDuration INT) ROW FORMAT DELIMITED FIELDS TERMINATED BY \\\",\\\" " \
    "STORED AS TEXTFILE LOCATION \\\"/user/%s/benchmark/scratch\\\";\"" \
    % (opts.sparksql_home, opts.sparksql_master_url, opts.sparksql_user))

  ssh_sparksql(
    "%s/bin/spark-sql --master %s " \
    "-e \"DROP TABLE IF EXISTS uservisits; " \
    "CREATE EXTERNAL TABLE uservisits (sourceIP STRING,destURL STRING," \
    "visitDate STRING,adRevenue DOUBLE,userAgent STRING,countryCode STRING," \
    "languageCode STRING,searchWord STRING,duration INT ) " \
    "ROW FORMAT DELIMITED FIELDS TERMINATED BY \\\",\\\" " \
    "STORED AS TEXTFILE LOCATION \\\"/user/%s/benchmark/uservisits\\\";\"" \
    % (opts.sparksql_home, opts.sparksql_master_url, opts.sparksql_user))

  ssh_sparksql(
    "%s/bin/spark-sql  --master %s " \
    "-e \"DROP TABLE IF EXISTS documents; " \
    "CREATE EXTERNAL TABLE documents (line STRING) STORED AS TEXTFILE " \
    "LOCATION \\\"/user/%s/benchmark/crawl\\\";\"" \
    % (opts.sparksql_home, opts.sparksql_master_url, opts.sparksql_user))

  print "=== FINISHED CREATING BENCHMARK DATA ==="

def main():
  opts = parse_args()

  if opts.sparksql:
    prepare_sparksql_dataset(opts)

  if opts.impala or opts.redshift or opts.hive or opts.hive_tez or opts.hive_cdh:
    print "Still not implemented"

if __name__ == "__main__":
  main()
