# Copyright 2013 The Regents of The University California
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Run a single query from the big data benchmark on a remote EC2 cluster.

   This will execute a single query from the benchmark multiple times
   and output percentile results. Note that to run an entire suite of
   queries, you'll need to wrap this script and call run multiple times.
"""

import subprocess
import sys
from sys import stderr
from optparse import OptionParser
import os
import time
import datetime
import re
import multiprocessing
from StringIO import StringIO
from pg8000 import DBAPI

# A scratch directory on your filesystem
LOCAL_TMP_DIR = "/tmp"

### Benchmark Queries ###
TMP_TABLE = "result"
TMP_TABLE_CACHED = "result_cached"
CLEAN_QUERY = "DROP TABLE %s;" % TMP_TABLE

# TODO: Factor this out into a separate file
QUERY_1a_HQL = "SELECT pageURL, pageRank FROM rankings WHERE pageRank > 1000"
QUERY_1b_HQL = QUERY_1a_HQL.replace("1000", "100")
QUERY_1c_HQL = QUERY_1a_HQL.replace("1000", "10")

QUERY_2a_HQL = "SELECT SUBSTR(sourceIP, 1, 8), SUM(adRevenue) FROM " \
                 "uservisits GROUP BY SUBSTR(sourceIP, 1, 8)"
QUERY_2b_HQL = QUERY_2a_HQL.replace("8", "10")
QUERY_2c_HQL = QUERY_2a_HQL.replace("8", "12")

QUERY_3a_HQL = """SELECT sourceIP,
                          sum(adRevenue) as totalRevenue,
                          avg(pageRank) as pageRank
                   FROM
                     rankings R JOIN
                     (SELECT sourceIP, destURL, adRevenue
                      FROM uservisits UV
                      WHERE UV.visitDate > "1980-01-01"
                      AND UV.visitDate < "1980-04-01")
                      NUV ON (R.pageURL = NUV.destURL)
                   GROUP BY sourceIP
                   ORDER BY totalRevenue DESC
                   LIMIT 1"""
QUERY_3a_HQL = " ".join(QUERY_3a_HQL.replace("\n", "").split())
QUERY_3b_HQL = QUERY_3a_HQL.replace("1980-04-01", "1983-01-01")
QUERY_3c_HQL = QUERY_3a_HQL.replace("1980-04-01", "2010-01-01")

QUERY_4_HQL = """DROP TABLE IF EXISTS url_counts_partial;
                 CREATE TABLE url_counts_partial AS
                   SELECT TRANSFORM (line)
                   USING "python UDFPATH/url_count.py" as (sourcePage,
                     destPage, count) from documents;
                 DROP TABLE IF EXISTS url_counts_total;
                 CREATE TABLE url_counts_total AS
                   SELECT SUM(count) AS totalCount, destpage
                   FROM url_counts_partial GROUP BY destpage;"""
QUERY_4_HQL = " ".join(QUERY_4_HQL.replace("\n", "").split())

QUERY_5_HQL = """DROP TABLE IF EXISTS url_counts_partial;
                 CREATE TABLE url_counts_partial AS
                   SELECT TRANSFORM (line)
                   USING "python UDFPATH/url_count.py" as (sourcePage,
                     destPage, count) from documents;
                 DROP TABLE IF EXISTS url_counts_total;
                 CACHE TABLE url_counts_total;
                 SELECT COUNT(1) FROM url_counts_total;
                 CREATE TABLE url_counts_total AS
                   SELECT SUM(count) AS totalCount, destpage
                   FROM url_counts_partial GROUP BY destpage;"""
QUERY_5_HQL = " ".join(QUERY_5_HQL.replace("\n", "").split())

QUERY_4_HQL_HIVE_UDF = """DROP TABLE IF EXISTS url_counts_partial;
                 CREATE TABLE url_counts_partial AS
                   SELECT TRANSFORM (line)
                   USING "python /tmp/url_count.py" as (sourcePage,
                     destPage, count) from documents;
                 DROP TABLE IF EXISTS url_counts_total;
                 CREATE TABLE url_counts_total AS
                   SELECT SUM(count) AS totalCount, destpage
                   FROM url_counts_partial GROUP BY destpage;"""
QUERY_4_HQL_HIVE_UDF = " ".join(QUERY_4_HQL_HIVE_UDF.replace("\n", "").split())

QUERY_1_PRE = "CREATE TABLE %s (pageURL STRING, pageRank INT);" % TMP_TABLE
QUERY_2_PRE = "CREATE TABLE %s (sourceIP STRING, adRevenue DOUBLE);" % TMP_TABLE
QUERY_3_PRE = "CREATE TABLE %s (sourceIP STRING, " \
    "adRevenue DOUBLE, pageRank DOUBLE);" % TMP_TABLE

QUERY_1a_SQL = QUERY_1a_HQL
QUERY_1b_SQL = QUERY_1b_HQL
QUERY_1c_SQL = QUERY_1c_HQL

QUERY_2a_SQL = QUERY_2a_HQL.replace("SUBSTR", "SUBSTRING")
QUERY_2b_SQL = QUERY_2b_HQL.replace("SUBSTR", "SUBSTRING")
QUERY_2c_SQL = QUERY_2c_HQL.replace("SUBSTR", "SUBSTRING")
QUERY_3a_SQL = """SELECT sourceIP, totalRevenue, avgPageRank
                    FROM
                      (SELECT sourceIP,
                              AVG(pageRank) as avgPageRank,
                              SUM(adRevenue) as totalRevenue
                      FROM Rankings AS R, UserVisits AS UV
                      WHERE R.pageURL = UV.destinationURL
                      AND UV.visitDate
                        BETWEEN Date('1980-01-01') AND Date('1980-04-01')
                      GROUP BY UV.sourceIP)
                   ORDER BY totalRevenue DESC LIMIT 1""".replace("\n", "")
QUERY_3a_SQL = " ".join(QUERY_3a_SQL.replace("\n", "").split())
QUERY_3b_SQL = QUERY_3a_SQL.replace("1980-04-01", "1983-01-01")
QUERY_3c_SQL = QUERY_3a_SQL.replace("1980-04-01", "2010-01-01")

def create_as(query):
  return "CREATE TABLE %s AS %s;" % (TMP_TABLE, query)
def insert_into(query):
  return "INSERT INTO TABLE %s %s;" % (TMP_TABLE, query)
def count(query):
  return query
  return "SELECT COUNT(*) FROM (%s) q;" % query

IMPALA_MAP = {'1a': QUERY_1_PRE, '1b': QUERY_1_PRE, '1c': QUERY_1_PRE,
              '2a': QUERY_2_PRE, '2b': QUERY_2_PRE, '2c': QUERY_2_PRE,
              '3a': QUERY_3_PRE, '3b': QUERY_3_PRE, '3c': QUERY_3_PRE}

TEZ_MAP =    {'1a':(count(QUERY_1a_HQL),), '1b':(count(QUERY_1b_HQL),), '1c': (count(QUERY_1c_HQL),),
              '2a':(count(QUERY_2a_HQL),), '2b':(count(QUERY_2b_HQL),), '2c': (count(QUERY_2c_HQL),),
              '3a':(count(QUERY_3a_HQL),), '3b':(count(QUERY_3b_HQL),), '3c': (count(QUERY_3c_HQL),)}

QUERY_MAP = {
             '1a':  (create_as(QUERY_1a_HQL), insert_into(QUERY_1a_HQL),
                     create_as(QUERY_1a_SQL)),
             '1b':  (create_as(QUERY_1b_HQL), insert_into(QUERY_1b_HQL),
                     create_as(QUERY_1b_SQL)),
             '1c':  (create_as(QUERY_1c_HQL), insert_into(QUERY_1c_HQL),
                     create_as(QUERY_1c_SQL)),
             '2a': (create_as(QUERY_2a_HQL), insert_into(QUERY_2a_HQL),
                    create_as(QUERY_2a_SQL)),
             '2b': (create_as(QUERY_2b_HQL), insert_into(QUERY_2b_HQL),
                    create_as(QUERY_2b_SQL)),
             '2c': (create_as(QUERY_2c_HQL), insert_into(QUERY_2c_HQL),
                    create_as(QUERY_2c_SQL)),
             '3a': (create_as(QUERY_3a_HQL), insert_into(QUERY_3a_HQL),
                    create_as(QUERY_3a_SQL)),
             '3b': (create_as(QUERY_3b_HQL), insert_into(QUERY_3b_HQL),
                    create_as(QUERY_3b_SQL)),
             '3c': (create_as(QUERY_3c_HQL), insert_into(QUERY_3c_HQL),
                    create_as(QUERY_3c_SQL)),
             '4':  (QUERY_4_HQL, None, None),
             '5':  (QUERY_5_HQL, None, None),
             '4_HIVE':  (QUERY_4_HQL_HIVE_UDF, None, None)}

# Turn a given query into a version using cached tables
def make_input_cached(query):
  return query.replace("uservisits", "uservisits_cached") \
              .replace("rankings", "rankings_cached") \
              .replace("url_counts_partial", "url_counts_partial_cached") \
              .replace("url_counts_total", "url_counts_total_cached") \
              .replace("documents", "documents_cached")

# Turn a given query into one that creats cached tables
def make_output_cached(query):
  return query.replace(TMP_TABLE, TMP_TABLE_CACHED)

### Runner ###
def parse_args():
  parser = OptionParser(usage="run_query.py [options]")

  parser.add_option("-m", "--impala", action="store_true", default=False,
      help="Whether to include Impala")
  parser.add_option("-s", "--sparksql", action="store_true", default=False,
      help="Whether to include Shark")
  parser.add_option("-r", "--redshift", action="store_true", default=False,
      help="Whether to include Redshift")
  parser.add_option("--hive", action="store_true", default=False,
      help="Whether to include Hive")
  parser.add_option("--tez", action="store_true", default=False,
      help="Use in conjunction with --hive")
  parser.add_option("--hive-cdh", action="store_true", default=False,
      help="Hive on CDH cluster")

  parser.add_option("-g", "--sparksql-no-cache", action="store_true",
      default=False, help="Disable caching in Shark")
  parser.add_option("-t", "--reduce-tasks", type="int", default=150,
      help="Number of reduce tasks in Spark")
#  parser.add_option("-z", "--clear-buffer-cache", action="store_true",
#      default=False, help="Clear disk buffer cache between query runs")

  parser.add_option("-b", "--sparksql-host",
      help="Hostname of SparkSQL master node")
  parser.add_option("-u", "--sparksql-user",
      help="User of SparkSQL master node")
  parser.add_option("-i", "--sparksql-home",
      help="Spark home")
  parser.add_option("-p", "--sparksql-master-url",
      help="SparkSQL master url")
  parser.add_option("-o", "--sparksql-ex-mem",
      help="SparkSQL executor memory")
  parser.add_option("-y", "--sparksql-identity-file",
      help="SSH private key file to use for logging into Shark node")
  parser.add_option("--num-trials", type="int", default=10,
      help="Number of trials to run for this query")
  parser.add_option("--prefix", type="string", default="",
      help="Prefix result files with this string")

  parser.add_option("-q", "--query-num", default="1a",
                    help="Which query to run in benchmark: " \
                    "%s" % ", ".join(QUERY_MAP.keys()))
  parser.add_option("-l", "--local-tmp-dir", default=LOCAL_TMP_DIR,
      help="Local tmp dir")
  parser.add_option("-k", "--remote-tmp-dir", default=LOCAL_TMP_DIR,
      help="Remote tmp dir")
  parser.add_option("-j", "--udf-path",
      help="Path to url_count.py")

  (opts, args) = parser.parse_args()

  if not (opts.impala or opts.sparksql or opts.redshift or opts.hive or opts.hive_cdh):
    parser.print_help()
    sys.exit(1)

  if opts.sparksql and (opts.sparksql_identity_file is None or
                     opts.sparksql_host is None or
                     opts.sparksql_user is None or
                     opts.sparksql_home is None or
                     opts.sparksql_master_url is None or
                     opts.udf_path is None):
    print >> stderr, \
        "SparkSQL requires identity file, hostname, user, SparkSQL home,\
        SparkSQL master url and udf path"
    sys.exit(1)

  if opts.query_num not in QUERY_MAP:
    print >> stderr, "Unknown query number: %s" % opts.query_num
    sys.exit(1)

  return opts

# Run a command on a host through ssh, throwing an exception if ssh fails
def ssh(host, username, identity_file, command):
  return subprocess.check_call(
      "ssh -t -o StrictHostKeyChecking=no -i %s %s@%s '%s'" %
      (identity_file, username, host, command), shell=True)

# Copy a file to a given host through scp, throwing an exception if scp fails
def scp_to(host, identity_file, username, local_file, remote_file):
  subprocess.check_call(
      "scp -q -o StrictHostKeyChecking=no -i %s '%s' '%s@%s:%s'" %
      (identity_file, local_file, username, host, remote_file), shell=True)

# Copy a file to a given host through scp, throwing an exception if scp fails
def scp_from(host, identity_file, username, remote_file, local_file):
  subprocess.check_call(
      "scp -q -o StrictHostKeyChecking=no -i %s '%s@%s:%s' '%s'" %
      (identity_file, username, host, remote_file, local_file), shell=True)

def run_sparksql_benchmark(opts):
  def ssh_sparksql(command):
    command = "source $HOME/.bash_profile; %s" % command
    ssh(opts.sparksql_host, opts.sparksql_user, opts.sparksql_identity_file, command)

  local_clean_query = CLEAN_QUERY
  local_query_map = QUERY_MAP

  prefix = str(time.time()).split(".")[0]
  query_file_name = "%s_workload.sh" % prefix
  slaves_file_name = "%s_slaves" % prefix
  local_query_file = os.path.join(opts.local_tmp_dir, query_file_name)
  local_slaves_file = os.path.join(opts.local_tmp_dir, slaves_file_name)
  query_file = open(local_query_file, 'w')
  remote_result_file = "%s/%s_results" % (opts.remote_tmp_dir, prefix)
  remote_tmp_file = "%s/%s_out" % (opts.remote_tmp_dir, prefix)
  remote_query_file = "%s/%s" % (opts.remote_tmp_dir, query_file_name)

  runner = "%s/bin/spark-sql --master %s " % (opts.sparksql_home,
      opts.sparksql_master_url)

  if opts.sparksql_ex_mem is not None:
    runner = "%s --executor-memory %s " % (runner, opts.sparksql_ex_mem)

  print "Getting Slave List"
  scp_from(opts.sparksql_host, opts.sparksql_identity_file, opts.sparksql_user,
           "%s/conf/slaves" % opts.sparksql_home, local_slaves_file)
  slaves = map(str.strip, open(local_slaves_file).readlines())

  print "Restarting standalone scheduler..."
  ssh_sparksql("%s/sbin/stop-all.sh" % opts.sparksql_home)
  ensure_spark_stopped_on_slaves(slaves)
  time.sleep(30)
  ssh_sparksql("%s/sbin/stop-all.sh" % opts.sparksql_home)
  ssh_sparksql("%s/sbin/start-all.sh" % opts.sparksql_home)
  time.sleep(10)

  # Two modes here: Shark Mem and Shark Disk. If using Shark disk clear buffer
  # cache in-between each query. If using Shark Mem, used cached tables.

  query_list = "set spark.sql.shuffle.partitions = %s;" % opts.reduce_tasks

  # Throw away query for JVM warmup
  query_list += "SELECT COUNT(*) FROM scratch;"

  # Create cached queries for Spark Mem
  if not opts.sparksql_no_cache:
    local_clean_query = make_output_cached(CLEAN_QUERY)

    def convert_to_cached(query):
      return (make_output_cached(make_input_cached(query[0])), )

    local_query_map = QUERY_MAP
    local_query_map.update((k, convert_to_cached(local_query_map.get(k))) for k
        in local_query_map.iterkeys())

    # Set up cached tables
    if '4' in opts.query_num:
      # Query 4 uses entirely different tables
      query_list += """
                    DROP TABLE IF EXISTS documents_cached;
                    CREATE TABLE documents_cached AS SELECT * FROM documents;
                    CACHE TABLE documents_cached;
                    SELECT COUNT(1) FROM documents_cached;
                    """
    elif '5' in opts.query_num:
      query_list += """
                    DROP TABLE IF EXISTS documents_cached;
                    CREATE TABLE documents_cached AS SELECT * FROM documents;
                    CACHE TABLE documents_cached;
                    SELECT COUNT(1) FROM documents_cached;
                    """
    elif opts.query_num in ['1a','1b','1c']:
      query_list += """
                    DROP TABLE IF EXISTS uservisits_cached;
                    DROP TABLE IF EXISTS rankings_cached;
                    CREATE TABLE rankings_cached AS SELECT * FROM rankings;
                    CACHE TABLE rankings_cached;
                    SELECT COUNT(1) FROM rankings_cached;
                    """
    elif opts.query_num in ['2a','2b','2c']:
      query_list += """
                    DROP TABLE IF EXISTS uservisits_cached;
                    DROP TABLE IF EXISTS rankings_cached;
                    CREATE TABLE uservisits_cached AS SELECT * FROM uservisits;
                    CACHE TABLE uservisits_cached;
                    SELECT COUNT(1) FROM uservisits_cached;
                    """
    else:
      query_list += """
                    DROP TABLE IF EXISTS uservisits_cached;
                    DROP TABLE IF EXISTS rankings_cached;
                    CREATE TABLE uservisits_cached AS SELECT * FROM uservisits;
                    CREATE TABLE rankings_cached AS SELECT * FROM rankings;
                    CACHE TABLE uservisits_cached;
                    CACHE TABLE rankings_cached;
                    SELECT COUNT(1) FROM uservisits_cached;
                    SELECT COUNT(1) FROM rankings_cached;
                    """

  # Warm up for Query 1
  if '1' in opts.query_num:
    query_list += "DROP TABLE IF EXISTS warmup;"
    query_list += "CREATE TABLE warmup AS SELECT pageURL, pageRank FROM scratch WHERE pageRank > 1000;"

  if ('4' not in opts.query_num) and ('5' not in opts.query_num):
    query_list += local_clean_query
  query_list += local_query_map[opts.query_num][0]

  query_list = re.sub("\s\s+", " ", query_list.replace('\n', ' ').
          replace('UDFPATH', opts.udf_path))

  print "\nQuery:"
  print query_list.replace(';', ";\n")

#  if opts.clear_buffer_cache:
#    query_file.write("python /root/sparksql/bin/dev/clear-buffer-cache.py\n")

  query_file.write(
    "%s -e '%s' > %s 2>&1\n" % (runner, query_list, remote_tmp_file))

  query_file.write(
      "cat %s | grep Time | grep -v INFO |grep -v MapReduce >> %s\n" % (
        remote_tmp_file, remote_result_file))

  query_file.close()

  print "Copying files to Spark"
  scp_to(opts.sparksql_host, opts.sparksql_identity_file, opts.sparksql_user,
      local_query_file, remote_query_file)
  ssh_sparksql("chmod 775 %s" % remote_query_file)

  # Run benchmark
  print "Running remote benchmark..."

  # Collect results
  results = []
  contents = []

  for i in range(opts.num_trials):
    print "Stopping Executors on Slaves....."
    ensure_spark_stopped_on_slaves(slaves)
    print "Query %s : Trial %i" % (opts.query_num, i+1)
    ssh_sparksql("%s" % remote_query_file)
    local_results_file = os.path.join(opts.local_tmp_dir, "%s_results" % prefix)
    scp_from(opts.sparksql_host, opts.sparksql_identity_file, opts.sparksql_user,
        "%s/%s_results" % (opts.remote_tmp_dir, prefix), local_results_file)
    content = open(local_results_file).readlines()
    all_times = map(lambda x: float(x.split(": ")[1].split(" ")[0]), content)

    if '4' in opts.query_num:
      query_times = all_times[-4:]
      part_a = query_times[1]
      part_b = query_times[3]
      print "Parts: %s, %s" % (part_a, part_b)
      result = float(part_a) + float(part_b)
    elif '5' in opts.query_num:
      query_times = all_times[-6:]
      part_a = query_times[1]
      part_b = query_times[5]
      print "Parts: %s, %s" % (part_a, part_b)
      result = float(part_a) + float(part_b)
    else:
      result = all_times[-1] # Only want time of last query

    print "Result: ", result
    print "Raw Times: ", content

    results.append(result)
    contents.append(content)

    # Clean-up
    #ssh_sparksql("rm /mnt/%s*" % prefix)
    print "Clean Up...."
    ssh_sparksql("rm %s/%s_results" % (opts.remote_tmp_dir, prefix))
    os.remove(local_results_file)

  os.remove(local_slaves_file)
  os.remove(local_query_file)

  return results, contents

def get_percentiles(in_list):
  def get_pctl(lst, pctl):
    return lst[int(len(lst) * pctl)]
  in_list = sorted(in_list)
  return "%s\t%s\t%s" % (
    get_pctl(in_list, 0.05),
    get_pctl(in_list, .5),
    get_pctl(in_list, .95)
  )

def ssh_ret_code(host, user, id_file, slave_host, cmd):
  try:
    return subprocess.check_call(
              "ssh -t -o StrictHostKeyChecking=no -i %s %s@%s 'ssh -t %s %s'" %
                    (id_file, user, host, slave_host, cmd), shell=True)
  except subprocess.CalledProcessError as e:
    return e.returncode

def ensure_spark_stopped_on_slaves(slaves):
  stop = False
  while not stop:
    cmd = "jps | grep ExecutorBackend"
    ret_vals = map(lambda s: ssh_ret_code(opts.sparksql_host, opts.sparksql_user,
      opts.sparksql_identity_file, s, cmd), slaves)
    print ret_vals
    if 0 in ret_vals:
      print "Spark is still running on some slaves... sleeping"
      cmd = "jps | grep ExecutorBackend | cut -d \" \" -f 1 | xargs -rn1 kill -9"
      map(lambda s: ssh_ret_code(opts.sparksql_host, opts.sparksql_user,
        opts.sparksql_identity_file, s, cmd), slaves)
      time.sleep(2)
    else:
      stop = True

def main():
  global opts
  opts = parse_args()

  print "Query %s:" % opts.query_num

  if opts.impala or opts.redshift or opts.hive or opts.hive_cdh:
    print "Not yet implemented"
  if opts.sparksql:
    results, contents = run_sparksql_benchmark(opts)

  if opts.impala:
    if opts.clear_buffer_cache:
      fname = "impala_disk"
    else:
      fname = "impala_mem"
  elif opts.sparksql and opts.sparksql_no_cache:
    fname = "sparksql_disk"
  elif opts.sparksql:
    fname = "sparksql_mem"
  elif opts.redshift:
    fname = "redshift"
  elif opts.hive:
    if opts.clear_buffer_cache:
      fname = "hive_clear_cache"
    else:
      fname = "hive"
  elif opts.hive_cdh:
    if opts.clear_buffer_cache:
      fname = "cdh_hive_clear_cache"
    else:
      fname = "cdh_hive"

  fname = opts.prefix + fname

  def prettylist(lst):
    return ",".join([str(k) for k in lst])

  output = StringIO()
  outfile = open('results_%s_%s_%s' % (fname, opts.query_num, datetime.datetime.now()), 'w')

  try:
    if not opts.redshift:
      print >> output, "Contents: \n%s" % str(prettylist(contents))
    print >> output, "=================================="
    print >> output, "Results: %s" % prettylist(results)
    print >> output, "Percentiles: %s" % get_percentiles(results)
    print >> output, "Best: %s"  % min(results)
    if not opts.redshift:
      print >> output, "Contents: \n%s" % str(prettylist(contents))
    print output.getvalue()
    print >> outfile, output.getvalue()
  except:
    print output.getvalue()
    print >> outfile, output.getvalue()

  output.close()
  outfile.close()

if __name__ == "__main__":
  main()
